/*
 * ATMega48
 * Battery +ve to vcc 7
 * Battery -ve to gnd 8
 * cap 103 across 7 & 8
 * pin 22 gnd battery -ve also
 * pin 20 avcc to battery +ve
 * 4.7KR from rst pin 1 to battery +ve
 * pot across battery terminals
 * centre pot to pin 23/PC0/ADC0
 *
 * LED Left anode to PB0/pin 14
 *	follow table in main
 *
 * LED cathode, every 3 pins. 180R to GND
 */

#define POT_PIN PC0

// speed of CPU for calculating delay calls
// CLKPR.CLKPSn[3:0] = 0011 - CPU clock = 8MHz/8 == 1MHz (page 63)
#define F_CPU 1000000UL

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <util/delay_basic.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/power.h>
#include <avr/sleep.h>

#include <util/delay.h>

// seed the RNG
//
// we need a different seed on each boot but we have no RTC so we resort to saving a random
// number from a previous run. I tried just incrementing the seed by 1 each time but the
// bits I looked at just alternated between runs - this works and I didn't investigate further
static void init_rng(void) {
	// 16 bits in the EEPROM to store the seed for next time
	static uint16_t EEMEM stored_seed;
	// location in SRAM to copy the seed to
	uint16_t seed;

	// read the seed from EEPROM - first time through this is probably 0 or something random but
	// thats a valid seed
	seed = eeprom_read_word(&stored_seed);
	// seed the RNG
	srand(seed);
	// store a seed for next time - only 16 bits are returned by rand() but we play it safe
	eeprom_write_word(&stored_seed, rand() & 0xFFFF);
}

// delay with a busy loop. only used for the starting animation and I haven't got the time or
// energy to use the timer and for a few seconds it probably doesn't matter. avr-libc delay
// requires compile time constants hence rolling our own
static void busy_delay(uint8_t ms) {

	while (ms--) {
		// 250 is a guess - i haven't calculated it but seems close enough
		_delay_loop_2(250);
	}
}

// flag to note that the ADC is finished. actual bit flag is cleared when the interrupt routine
// is fired so we record the infor here
static volatile bool adc_fired;
// interrupt routine called when ADC has finished taking a reading
ISR(ADC_vect) {
	adc_fired = true;
}

// struct to describe a pin we may want to toggle
struct pin_id {
	volatile uint8_t *port;
	uint8_t pin;
};

// set the pins connected to the LEDs to outputs
static void set_led_pins(struct pin_id *pins, uint8_t count) {
	// which DDR goes with which PORT
	struct port_mapping {
		volatile uint8_t *port;
		volatile uint8_t *direction_port;
	} port_mapping[] = {
		{ &PORTB, &DDRB },
		{ &PORTC, &DDRC },
		{ &PORTD, &DDRD }
	};

	// loop through each pin
	for (uint8_t pin_i = 0; pin_i < count; pin_i++) {
		// find the DDR register for this port
		for (uint8_t mapping_i = 0;
				mapping_i < (sizeof(port_mapping) / sizeof(port_mapping)[0]);
				mapping_i++) {
			if (port_mapping[mapping_i].port == pins[pin_i].port) {
				// set the direction pin
				*(port_mapping[mapping_i].direction_port) |= _BV(pins[pin_i].pin);
				break;
			}
		}
	}
}

// configure the peripherals we want to use
static void setup_peripherals(struct pin_id *pins, uint8_t count) {

	// set up LED pins as outputs
	set_led_pins(pins, count);

	// enable the pull up resistors for unused pins
	PORTD |= _BV(PIND4);

	// set ADMUX register (page 324)
	//
	// REFS[1:0] = 01 - AVcc with external capacitor at AREF pin
	// ADLAR = 1 - put the important ADC bits in ADCH so we can skip reading ADCL (8 bit ADC)
	// MUX[3:0] = 0000 - use ADC0 in single ended mode
	ADMUX |= _BV(REFS1)
			| _BV(ADLAR);
			
	// set ADCSRA register (page 326)	
	//
	// ADEN = 1 - enable ADC
	// ADIE = 1 - enable ADC interrupts
	// ADPS[2:0] = 111 - divide clock by 128 = 62.5Hz (should be between 50 and 200Hz for best
	//	results)
	ADCSRA = _BV(ADEN) // enable ADC
			| _BV(ADIE) // enable interupts from the ADC
			| _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0); // divide clock by 128

	// disable stuff we don't use to save power
	PRR |= _BV(PRTWI)
			| _BV(PRTIM2)
			| _BV(PRTIM1)
			| _BV(PRTIM0)
			| _BV(PRSPI)
			| _BV(PRUSART0);

	// disable analogue comparator
	ACSR &= (uint8_t)~(_BV(ACIE)); // disable interrupts to stop spurious signals
	ACSR |= _BV(ACD); // and disable comparator

	// don't use the ADC pin as a digital input whilst it gets an analogue signal (page 333)
	DIDR0 |= _BV(ADC0D);
}

// turn on 1 pin and switch off everything else
//
// if invert is true then turn on everything but one pin
// count is the number of elements in pins
static void light_1_pin(uint8_t index, bool invert, struct pin_id *pins, uint8_t count) {

	// for each pin
	for (uint8_t pin_i = 0; pin_i < count; pin_i++) {
		if (invert ^ (pin_i == index)) {
			// light this pin
			*(pins[pin_i].port) |= _BV(pins[pin_i].pin);
		} else {
			// turn this one off
			*(pins[pin_i].port) &= (uint8_t)~(_BV(pins[pin_i].pin));
		}
	}
}

// turn on 2 pins and switch off everything else
//
// if invert is true then turn on everything but one pin
// count is the number of elements in pins
static void light_2_pin(uint8_t index1, uint8_t index2, bool invert, struct pin_id *pins,
		uint8_t count) {

	// for each pin
	for (uint8_t pin_i = 0; pin_i < count; pin_i++) {
		if (invert ^ ((pin_i == index1) || (pin_i == index2))) {
			// light this pin
			*(pins[pin_i].port) |= _BV(pins[pin_i].pin);
		} else {
			// turn this one off
			*(pins[pin_i].port) &= (uint8_t)~(_BV(pins[pin_i].pin));
		}
	}
}

// turn on all pins up to index
//
// left controls from which end we start
// count is the number of elements in pins
static void light_bar(uint8_t index, bool left, struct pin_id *pins, uint8_t count) {

	// for each pin
	for (uint8_t pin_i = 0; pin_i < count; pin_i++) {
		if ((left && (pin_i <= index)) ||
				(!left && (pin_i >= index))) {
			// light this pin
			*(pins[pin_i].port) |= _BV(pins[pin_i].pin);
		} else {
			// turn this one off
			*(pins[pin_i].port) &= (uint8_t)~(_BV(pins[pin_i].pin));
		}
	}
}

// turn off all LEDs
static void blank(struct pin_id *pins, uint8_t count) {

	for (uint8_t pin_i = 0; pin_i < count; pin_i++) {
		// turn this one off
		*(pins[pin_i].port) &= (uint8_t)~(_BV(pins[pin_i].pin));
	}
}

// turn on pins in order
//
// forwards controls the end we start and direction
// delay is how long to wait before moving on in ms (approx)
// invert means what would be on is off and vice versa
// count is the number of elements in pins
static void chase(bool forwards, uint8_t delay, bool invert, struct pin_id *pins, uint8_t count) {

	// for each pin
	for (uint8_t j = 0; j < count; j++) {
		// determine which pin we need to light
		uint8_t pin_to_light;
		if (forwards) {
			pin_to_light = j;
		} else {
			pin_to_light = 19 - j;
		}
		// light it
		light_1_pin(pin_to_light, invert, pins, count);
		// pause
		busy_delay(delay);
	}
}

// chase in both directions at once
static void cross(uint8_t delay, bool invert, struct pin_id *pins, uint8_t count) {

	for (uint8_t i = 0; i < count; i++) {
		light_2_pin(i, count - 1 - i, invert, pins, count);
		busy_delay(delay);
	}
}

// play a random animation at power on
static void startup_animation(struct pin_id *pins, uint8_t count) {
	// get a random number
	uint16_t r = rand();

	// the top bit is always 0 but we use the next 2 bits to decide which animation to use
	uint8_t animation = (r >> 13) & 0x3;
	// and the next bit to decide on fast or slow
	bool fast = (r >> 12) & 1;
	
	// determine repetition and speed
	uint8_t repetition = 2;
	uint8_t speed = 100;
	if (fast) {
		repetition = 3;
		speed = 25;
	}

	// play the animation
	for (uint8_t i = 0; i < repetition; i++) {
		switch (animation) {
		case 0:
			cross(speed, true, pins, count);
			break;
		case 1:
			cross(speed, false, pins, count);
			break;
		case 2:
			chase(true, speed, false, pins, count);
			chase(false, speed, false, pins, count);
			break;
		case 3:
			chase(true, speed, true, pins, count);
			chase(false, speed, true, pins, count);
			break;
		}
	}

	// switch off all the pins now we are done
	blank(pins, count);
}

// move bar graph LED based on slider
int main(void) {
	// pins of LEDs in order
	struct pin_id pins[20] = {
		{ &PORTB, PB0 },
		{ &PORTD, PD7 },
		{ &PORTB, PB1 },
		{ &PORTB, PB2 },
		{ &PORTB, PB3 },
		{ &PORTB, PB4 },
		{ &PORTB, PB5 },
		{ &PORTD, PD6 },
		{ &PORTD, PD5 },
		{ &PORTB, PB7 },
		{ &PORTB, PB6 },
		{ &PORTC, PC1 },
		{ &PORTC, PC2 },
		{ &PORTC, PC3 },
		{ &PORTC, PC4 },
		{ &PORTC, PC5 },
		{ &PORTD, PD0 },
		{ &PORTD, PD1 },
		{ &PORTD, PD2 },
		{ &PORTD, PD3 }
	};

	// call srand
	init_rng();

	// configure what we will be using
	setup_peripherals(pins, sizeof(pins) / sizeof(pins[0]));

	// play a random animation
	startup_animation(pins, sizeof(pins) / sizeof(pins[0]));

	// decide which side to use for the bar graph
	// rand returns 15 bits of randomness so we use the highest bit
	bool side = (rand() >> 14) & 0x01;

	// when we sleep we can switch off almost everything but the ADC to save power
	set_sleep_mode(SLEEP_MODE_ADC);

	// enable interrupts
	sei();

	// loop forever reading the pot setting and updating the display
	for (;;) {
		// wait for the ADC to be ready
		do {
			adc_fired = false;
			sleep_mode();
		} while (!adc_fired);

		// light the right LEDs
		// 
		// we could optimise by only doing this if there was a change but it makes little
		// difference
		light_bar(ADCH/13, side, pins, sizeof(pins) / sizeof(pins[0]));
	}
}
