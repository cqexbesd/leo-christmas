/*
 * ATTiny85.
 * Battery +ve to 8
 * Battery -ve to 4
 * 103 cap from 4 to 8
 * Pin 1 (RESET) tied to battery +ve (pin 8) via 4.7KR
 * Pin 7 to centre of pot (ADC)
 * pin 5 to 47ohm R (PWM)
 * POT connected across battery terminals
 * resistor to transistor base
 * transistor collector to 6V +ve
 * transistor emittor to diode anode
 * diode canode to fan +ve
 * fan -ve to 6V -ve
 *
 * transistor drops 0.5V and diode 0.6V so fan gets 4.9V (wants 5V). If 6V is a bit over we
 * have 0.1V margin
 */
// fan output is PB0 aka OC0A
#define FAN_PIN PB0
// pot input is PB2 aka ADC1
#define POT_PIN PB2

#include <stdbool.h>
#include <stdint.h>

// speed of CPU for calculating delay calls
// CLKSEL[3:0] = 0010 - CPU clock = 8MHz/8 == 1MHz (page 148)
#define F_CPU 1000000UL

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/power.h>
#include <avr/sleep.h>

#include <util/delay.h>

// note that the ADC is finished as actual flag is cleared when the interrupt routine is fired
static volatile bool adc_fired;
// called when ADC has finished taking a reading
ISR(ADC_vect) {
	adc_fired = true;
}

static void setup_peripherals(void) {

	// set PORTB register (page 64)
	//
	// most things will be unused so an input and we need the pull-up resistors active to make
	// sure they don't float. the fan will be an output though and we don't want that on.
	PORTB = (uint8_t)~(_BV(FAN_PIN));
	
	// set DDRB register (page 64)
	//
	// FAN_PIN = 1 - make pin output
	// all else 0 - input (default)
	DDRB |= _BV(FAN_PIN);

	// set ADMUX register (page 134)
	//
	// REFS1 = 0, REFS0 = 0 - ADC reference voltage is Vcc
	// ADLAR = 1 - put the important ADC bits in ADCH so we can skip reading ADCL (8 bit ADC)
	// MUX[3:0] = 0001 - use ADC1 single ended mode
	ADMUX = (uint8_t)
			_BV(ADLAR)	// left align ADC result
			| _BV(MUX0);	// ADC1 single ended

	// set ADCSRA register (page 136)
	//
	// ADEN = 1 - enable ADC
	// ADATE = 0 - don't do continuous measurements (we will do one on every sleep)
	// ADIE = 1 - enable interrupts when the ADC is ready to wake us from sleep
	// ADPS[2:0] = 111 - divide clock by 128 = 62.5Hz (should be between 50 and 200Hz for best
	//		results)
	ADCSRA = (uint8_t)
			_BV(ADEN) // enable ADC
			| _BV(ADIE) // enable interupts from the ADC
			| _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0); // divide clock by 128

	// dont use the ADC pin as a digital input whilst it gets an analogue signal (page 138)
	DIDR0 |= (uint8_t)_BV(ADC1D);

	// disable analogue comparator (page 120) - don't use it so save power
	ACSR &= ~(_BV(ACIE)); // turn off comparator interupts to prevent false triggers
	ACSR |= _BV(ACD); // turn off comparator

	// set TCCR0A register (page 77)
	//
	// COM0A[1:0] = 10 - non-inverted PWM output
	// WGM0[1:0] = 11 - (with TCCR0B:WGM0[2] == 0) fast PWM using 0xFF as TOP
	TCCR0A |= _BV(COM0A1)
				| _BV(WGM01) | _BV(WGM00);

	// set TCCR0B register (page 79)
	//
	// WGM0[2] = 0 - (with TCCR0A:WGM0[2:1]) fast PWM using OxFF as TOP
	// CS0[2:0] = 100 - PWM generator clocked at clock/1024
	TCCR0B |= _BV(CS02) | _BV(CS00);
}

// fan speed control based on pot value
int main(void) {

	// set up hardware 
	setup_peripherals();

	// enable interrupts
	sei();

	// kick start the fan so it has some momentum in case initial value is too slow
	OCR0A = 254;
	_delay_ms(750);

	// main loop
	for (;;) {
		// sleep until we have an ADC reading. ADC only starts when we sleep so we can't have a
		// race condition but we check the right interrupt woke us
		do {
			adc_fired = false;
			sleep_mode();
		} while (!adc_fired);

		// ADC gives us 256 steps but the fan can only go between 47 and 254 (207 steps) -
		// exerimentally determined.
		// Therefore each ADC step is worth 0.81 of a PWM step (207/256) and we can convert
		// simply by multiplying ADC by 0.81.
		//
		// We don't want to use floating point maths however. Instead we multiply things by 16
		// so 0.81 becomes 13. We then divide by 16 at the end (which is just a right shift).
		// This requires 16 bit maths but the AVR can multiply 2 uint8_ts to get an uint16_t in
		// hardware. After the divide we are back in 8 bit range.
		//
		// We also have to shift the value from the 0 - 207 range to the 47 - 254 range so we
		// add 47 at the end.
		// 
		// We used to start from 45 and then, due to our approximations of 209/256, the
		// highest value this can give us is 252 so we special case anything over 200
		// (arbitrary limit) to give it a bit more power - this is no longer true
		uint8_t pwm_steps = ADCH * 13U / 16U;
		pwm_steps += 47;
		//if (pwm_steps > 200) {
		//	pwm_steps += 47;
		//} else {
		//	pwm_steps += 47;
		//}

		// set PWM compare value to calculated value
		OCR0A = pwm_steps;
	}

	return 0;
}
